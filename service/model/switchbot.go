package model

import "time"

type SwitchBotStatus struct {
	MacAddress string
	On         bool
	Updated    time.Time
}
