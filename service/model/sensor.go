package model

import "time"

type RpzSensorLog struct {
	Temperature1 float64 `csv:"Temp ch1"`
	Temperature2 float64 `csv:"Temp ch2"`
	Pressure1    float64 `csv:"Pressure ch1"`
	Pressure2    float64 `csv:"Pressure ch2"`
	Humidity1    float64 `csv:"Humidity ch1"`
	Humidity2    float64 `csv:"Humidity ch2"`
	Lux          float64 `csv:"Lux"`
	Timestamp    time.Time
}
