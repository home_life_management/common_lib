package database

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/home_life_management/common_lib/service/model"
)

type DatabaseOption struct {
	// required
	Name     string
	User     string
	Password string

	// not required
	Host string // default: localhost
	Port int    // default: 5432
}

type ButtonPushType int

const (
	Single ButtonPushType = iota
	Double
	Long
)

func (o *ButtonPushType) UnmarshalText(b []byte) error {
	str := strings.Trim(string(b), `"`)

	switch {
	case str == "single":
		*o = Single
	case str == "double":
		*o = Double
	case str == "long":
		*o = Long

	default:
		return errors.New("failed to parse ButtonPushType string as json")
	}

	return nil
}

type ButtonPushLog struct {
	Serial   string
	PushType ButtonPushType
	Created  *time.Time
}

// TODO: interfaceは依存する側のソースで定義するべきなので消す(interface分離原則)
type DatabaseProvider interface {
	Connect() error
	AddRpzSensorLog(model.RpzSensorLog) error
	GetLatestRpzSensorLog() (model.RpzSensorLog, error)
	GetRpzSensorLogOfMin(time.Time) (model.RpzSensorLog, error)
	GetBedTime(time.Time, int) (time.Time, error)
	SetSwitchBotStatus(macAddress string, on bool) error
	GetSwitchBotStatus(macAddress string) (model.SwitchBotStatus, error)
	Close()
}

type PostgresService struct {
	pool *pgxpool.Pool
	opt  *DatabaseOption
}

func NewPostgresService(o *DatabaseOption) (*PostgresService, error) {
	s := PostgresService{}
	v, err := isValidOption(o)
	if !v {
		return nil, err
	}
	setDefaultOption(o)
	s.opt = o

	return &s, nil
}

func (p *PostgresService) Connect() error {
	url := fmt.Sprintf("postgres://%s:%s@%s:%d/%s", p.opt.User, p.opt.Password, p.opt.Host, p.opt.Port, p.opt.Name)
	pool, err := pgxpool.Connect(context.Background(), url)
	if err != nil {
		return err
	}
	p.pool = pool

	return nil
}

func (p *PostgresService) AddRpzSensorLog(log model.RpzSensorLog) error {
	sqlStr := "INSERT INTO rpz_sensor (temperature1, temperature2, pressure1, pressure2, humidity1, humidity2, brightness, timestamp) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)"
	_, err := p.pool.Exec(context.Background(), sqlStr, log.Temperature1, log.Temperature2, log.Pressure1, log.Pressure2, log.Humidity1, log.Humidity2, log.Lux, time.Now())
	if err != nil {
		return err
	}

	return err
}

func (p *PostgresService) GetLatestRpzSensorLog() (model.RpzSensorLog, error) {
	var log model.RpzSensorLog
	sqlStr := "SELECT temperature1, temperature2, pressure1, pressure2, humidity1, humidity2, brightness, timestamp FROM rpz_sensor ORDER BY id DESC LIMIT 1"
	r := p.pool.QueryRow(context.Background(), sqlStr)
	err := r.Scan(&log.Temperature1, &log.Temperature2, &log.Pressure1, &log.Pressure2, &log.Humidity1, &log.Humidity2, &log.Lux, &log.Timestamp)
	if err != nil {
		return model.RpzSensorLog{}, err
	}

	return log, err
}

// TODO: 指定分のデータを取得するメソッドだが使用していないため不要なら削除
func (p *PostgresService) GetRpzSensorLogOfMin(min time.Time) (model.RpzSensorLog, error) {
	t := min.Truncate(60 * time.Second)

	var log model.RpzSensorLog
	sqlStr := "SELECT temperature1, temperature2, pressure1, pressure2, humidity1, humidity2, brightness FROM rpz_sensor WHERE DATE_TRUNC('minutes', timestamp) = $1"
	r := p.pool.QueryRow(context.Background(), sqlStr, t.Format("2006-01-02 15:04:00"))
	err := r.Scan(&log.Temperature1, &log.Temperature2, &log.Pressure1, &log.Pressure2, &log.Humidity1, &log.Humidity2, &log.Lux)
	if err != nil {
		return model.RpzSensorLog{}, err
	}

	return log, err
}

func (p *PostgresService) GetBedTime(date time.Time, luxThreshold int) (time.Time, error) {
	var bedTime time.Time
	d := date.Format("2006-01-02")
	sqlStr := "SELECT MAX(timestamp) FROM rpz_sensor WHERE DATE(timestamp) = $1 AND brightness > $2"
	err := p.pool.QueryRow(context.Background(), sqlStr, d, luxThreshold).Scan(&bedTime)
	if err != nil {
		return time.Time{}, err
	}

	return bedTime, nil
}

func (p *PostgresService) SetSwitchBotStatus(macAddress string, on bool) error {
	// 当該macのレコードが存在しない場合はinsert, 存在する場合はupdate
	sqlStr := `INSERT INTO switchbot(mac_address, switch_on, updated)
VALUES ($1, $2, $3)
ON CONFLICT(mac_address)
DO UPDATE SET
switch_on = $2,
updated = $3`
	_, err := p.pool.Exec(context.Background(), sqlStr, macAddress, on, time.Now())

	return err
}

func (p *PostgresService) GetSwitchBotStatus(macAddress string) (model.SwitchBotStatus, error) {
	var status model.SwitchBotStatus
	sqlStr := "SELECT mac_address, switch_on, updated FROM switchbot WHERE mac_address = $1"
	r := p.pool.QueryRow(context.Background(), sqlStr, macAddress)
	err := r.Scan(&status.MacAddress, &status.On, &status.Updated)
	if err != nil {
		return model.SwitchBotStatus{}, err
	}

	return status, err
}

func (p *PostgresService) AddLteButtonLog(log ButtonPushLog) error {
	// 本来現在時刻はcurrent_timestamp指定でinsert可能。
	// しかし何故かラズパイのdockerコンテナ内の時刻が1970年になってしまうため、time.Now()で現在時刻を取得して挿入
	// 参考) https://github.com/dbhi/qus/issues/6
	sqlStr := "INSERT INTO lte_button_log (device_serial, push_type, created) VALUES ($1, $2, $3)"
	_, err := p.pool.Exec(context.Background(), sqlStr, log.Serial, log.PushType, time.Now())
	return err
}

func (p *PostgresService) GetLatestLteButtonLog() (*ButtonPushLog, error) {
	sqlStr := "SELECT device_serial, push_type, created FROM lte_button_log ORDER BY created DESC LIMIT 1"
	var log ButtonPushLog
	err := p.pool.QueryRow(context.Background(), sqlStr).Scan(&log.Serial, &log.PushType, &log.Created)
	if err != nil {
		return nil, err
	}

	return &log, nil
}

func (p *PostgresService) GetLightedMinsAfter(start time.Time, luxThreshold int) (int, error) {
	ss := start.Format("15:04")
	sqlStr := "SELECT COUNT(*) FROM rpz_sensor WHERE brightness > $1 AND timestamp::date = current_timestamp::date AND timestamp::time > $2"
	var mins int
	err := p.pool.QueryRow(context.Background(), sqlStr, luxThreshold, ss).Scan(&mins)
	if err != nil {
		return -1, err
	}

	return mins, nil
}

func (p *PostgresService) Close() {
	p.pool.Close()
}

func setDefaultOption(o *DatabaseOption) {
	if o.Host == "" {
		o.Host = "localhost"
	}
	if o.Port == 0 {
		o.Port = 5432
	}
}

func isValidOption(o *DatabaseOption) (bool, error) {
	if o.Name == "" {
		return false, errors.New("required option Name not set to DatabaseOption")
	} else if o.User == "" {
		return false, errors.New("required option User not set to DatabaseOption")
	} else if o.Password == "" {
		return false, errors.New("required option Password not set to DatabaseOption")
	}

	return true, nil
}
