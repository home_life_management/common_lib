package switchbot

import (
	"errors"
	"os"
	"os/exec"

	"gitlab.com/home_life_management/common_lib/service/model"
)

type SwitchBotProvider interface {
	Toggle()
}

type SwitchBotDBProvider interface {
	SetSwitchBotStatus(macAddress string, on bool) error
	GetSwitchBotStatus(macAddress string) (model.SwitchBotStatus, error)
}

type SwitchBotService struct {
	macAddress string
	db         SwitchBotDBProvider
	scriptPath string
}

func NewSwitchBotService(macAddress string, db SwitchBotDBProvider, scriptPath string) (*SwitchBotService, error) {
	if _, err := os.Stat(scriptPath); errors.Is(err, os.ErrNotExist) {
		return nil, err
	}

	s := &SwitchBotService{
		macAddress: macAddress,
		db:         db,
		scriptPath: scriptPath,
	}
	return s, nil
}

func (s *SwitchBotService) Toggle() error {
	st, err := s.db.GetSwitchBotStatus(s.macAddress)
	if err != nil {
		return err
	}

	var target string
	if st.On {
		target = "off"
	} else {
		target = "on"
	}
	cmd := exec.Command("/usr/bin/python3", s.scriptPath, "-d", s.macAddress, "-c", target)
	err = cmd.Run()
	if err != nil {
		return err
	}

	err = s.db.SetSwitchBotStatus(s.macAddress, !st.On)
	if err != nil {
		return err
	}

	return nil
}
