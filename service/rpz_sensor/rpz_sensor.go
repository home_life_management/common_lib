package rpz

import (
	"os"
	"os/exec"

	"github.com/gocarina/gocsv"
	"gitlab.com/home_life_management/common_lib/service/database"
	"gitlab.com/home_life_management/common_lib/service/model"
)

type RpzSensorProvider interface {
	GetCurrentData() *model.RpzSensorLog
}

type RpzSensorService struct {
	db         database.DatabaseProvider
	scriptPath string
	logTmpPath string
}

type RpzSensorOption struct {
	// not required
	ScriptPath string // default: "/usr/local/bin/lib/rpz-sensor/python3/rpz_sensor.py"
	LogTmpPath string // default: "/var/log/sensor/tmp.log"
}

func NewRpzSensorService(db database.DatabaseProvider) *RpzSensorService {
	return &RpzSensorService{
		db:         db,
		scriptPath: "/usr/local/bin/lib/rpz-sensor/python3/rpz_sensor.py",
		logTmpPath: "/var/log/sensor/tmp.log",
	}
}

func NewRpzSensorServiceWithOption(db database.DatabaseProvider, o RpzSensorOption) *RpzSensorService {
	return &RpzSensorService{
		db:         db,
		scriptPath: o.ScriptPath,
		logTmpPath: o.LogTmpPath,
	}
}

func (r *RpzSensorService) GetCurrentData() (*model.RpzSensorLog, error) {
	cmd := exec.Command("/usr/bin/python3", r.scriptPath, "-l", r.logTmpPath)
	err := cmd.Run()
	if err != nil {
		return nil, err
	}
	defer os.Remove(r.logTmpPath)

	f, err := os.Open(r.logTmpPath)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	l := []*model.RpzSensorLog{}
	err = gocsv.UnmarshalFile(f, &l)
	if err != nil {
		return nil, err
	}

	return l[0], nil
}
