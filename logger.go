package common_lib

import (
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type LogLevel int

const (
	Debug LogLevel = 0
	Info  LogLevel = iota
	Warn
	Error
)

type LoggerOption struct {
	LogLevel LogLevel
}

func NewLogger() *zap.Logger {
	conf := getDefaultConfig()
	logger, _ := conf.Build()

	return logger
}

func NewLoggerWithOption(option LoggerOption) *zap.Logger {
	conf := getDefaultConfig()
	switch option.LogLevel {
	case Debug:
		conf.Level = zap.NewAtomicLevelAt(zap.DebugLevel)
	case Info:
		conf.Level = zap.NewAtomicLevelAt(zap.InfoLevel)
	case Warn:
		conf.Level = zap.NewAtomicLevelAt(zap.WarnLevel)
	case Error:
		conf.Level = zap.NewAtomicLevelAt(zap.ErrorLevel)
	}
	logger, _ := conf.Build()

	return logger
}

func timeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	const layout = "2006-01-02T15:04:05Z0700"
	enc.AppendString(t.Format(layout))
}

func getDefaultConfig() *zap.Config {
	conf := zap.Config{
		Level:            zap.NewAtomicLevelAt(zap.InfoLevel),
		Encoding:         "console",
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stdout"},
		EncoderConfig: zapcore.EncoderConfig{
			LevelKey:     "level",
			TimeKey:      "time",
			MessageKey:   "msg",
			CallerKey:    "caller",
			EncodeTime:   timeEncoder,
			EncodeLevel:  zapcore.LowercaseLevelEncoder,
			EncodeCaller: zapcore.ShortCallerEncoder,
		},
	}

	return &conf
}
