#!/bin/bash

set -eux

install_docker() {
  curl -sSL https://get.docker.com | sh
}

setup_db() {
  mkdir -p  /etc/home_life_management/
  cp -f docker-compose.yaml /etc/home_life_management/
  cp -rf sql /etc/home_life_management/
  cd /etc/home_life_management/
  docker compose up -d
}

main() {
  install_docker
  setup_db
}

main
