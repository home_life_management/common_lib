CREATE TABLE IF NOT EXISTS rpz_sensor
(
  id            bigserial NOT NULL PRIMARY KEY,
  temperature1  double precision,
  temperature2  double precision,
  pressure1     double precision,
  pressure2     double precision,
  humidity1     double precision,
  humidity2     double precision,
  brightness    double precision,
  timestamp     timestamp with time zone NOT NULL
);

CREATE TABLE IF NOT EXISTS switchbot
(
  mac_address   text NOT NULL PRIMARY KEY,
  switch_on     boolean NOT NULL,
  updated       timestamp with time zone NOT NULL
  -- TODO: timestampに現在時刻を入れる方法調査。下記はNG
  -- CURRENT_TIMESTAMP, now(): transaction開始時点の時刻。1950年等になる
  -- clock_timestamp(): 現在時刻らしいがなぜか1970年になる
);

CREATE TABLE IF NOT EXISTS lte_button_log
(
  device_serial_number  text NOT NULL,
  push_type             int  NOT NULL,
  created               timestamp with time zone NOT NULL
);